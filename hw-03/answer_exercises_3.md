3. [Horizontal Pod Autoscaler] Crea un objeto de kubernetes HPA, que escale a partir de las  métricas  CPU  o  memoria  (a  vuestra  elección).  Establece  el  umbral  al  50%  de CPU/memoria utilizada, cuando pase el umbral, automáticamente se deberá escalar al doble de replicas.
- En la carpeta "answer_exercise_3/" se encuentra el archivo answer_exercise_3.yaml y el documento .pdf con todas las capturas del proceso.

- Primeramente, rescataremos un archivo yaml de una practica anterior con un objeto Service y un objeto Deployment con un ReplicaSet con 3 replicas. Además, le añadiremos un objeto HorizontalPOodAutoscaler que setearemos con las siguientes características:
scaleUp:
      stabilizationWindowSeconds: 0
      policies:
      - type: Percent
        value: 100
        periodSeconds: 15
      selectPolicy: Max

Esto significa que cuando necesite escalar hacia arriba, replicará el pod, por lo tanto doblará el número de pods que superen el umbral que hemos fijado en el yaml, en nuestro caso del 50%.

Antes de pasar a crear nuestros objetos verificaremos que minikube tiene el plugin de metrics-server activado, por que sino no podrá verificar el porcentage de uso por pod que se está llevando a cabo. Ejecutamos el siguiente comando y buscamos metrics-server:
-> minikube addons list
Y si no esta activado ejecutamos lo siguiente:
-> minikube addons enable metrics-server

El siguiente paso será crear los objetos, para lo que ejecutaremos la siguiente instrucción:
-> kubectl apply -f answer_exercises_3.yaml
Ejecutaremos tambien:
-> kubectl get all
Para verificar que todo se ha creado correctamente.

Ahora ejecutaremos dos comandos más para ir monitorizando que todo se ejecuta correctamente. El primero es para ver en tiempo real los pods creados y su estado, y el segundo es para verificar el porcentage de uso del HorizontalPodAutoscaler y los logs del status que esta llevando a cabo y sus acciones:
-> watch kubectl get po
-> watch kubectl describe hpa nginx-hpa

Lo último será ejecutar el siguiente comando para que aumente el uso de cpu haciendo peticiones a los pods:
-> kubectl  run  -i  --tty  load-generator  --rm  --image=busybox  --restart=Never  -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://nginx-service; done"

En este momento, a través de la monitorización, podemos observar como el uso de cpu por pod va en aumento y en el momento de superar el 50% de uso y mantenerse durante un corto periodo de tiempo, el sistema replicará los pods que excedan este uso para mitigar el uso de cpu y que la media de uso de la cpu de los pods, baje.
Por lo tanto, veremos como se doblan el número de pods después de pasar el umbral y veremos como después de la replicación baja el uso de cpu.

- En el archivo .pdf de la carpeta "answer_exercise_3/" se encuentra el resto de capturas de pantalla con lo que se ha llevado a cabo.