2. [StatefulSet] Crear un StatefulSet con 3 instancias de MongoDB (ejemplo visto en clase) 
Se pide: 

- En la carpeta "answer_exercise_2/" se encuentra el archivo answer_exercise_2.yaml y el documento .pdf con todas las capturas del proceso.
 
• Habilitar el clúster de MongoDB 
    - Primeramente, contruiremos un archivo yaml con la configuración de nuestro StatefulSet y un servicio por encima que lo controle. Además, configuraremos el StatefulSet con 3 replicas y un sistema basado en "master/slave" en que un pod hará de "master" y dos pods que aran de "slaves" donde se replicarán los datos y en caso de que el pod "master" caiga, uno de los pods "slaves" se convertirá en "master" y cuando el antiguo "master" se vuelva a levantar lo hara como "slave". Así, conseguiremos que los datos sean seguros y persistentes en el tiempo previniendo posibles errores o pérdidas. Como contrapartida, la gestión de las réplicas con estado, que es nuestro caso, puede ocasionar una tardanza mayor que si lo hicieramos con un ReplicaSet, ya que los pods necesitan mantener un identificador único para nuestro propósito.
    Por lo tanto, una vez construido el yaml levantamos los objetos con el siguiente comando:
    -> kubectl apply -f answer_exercise_2.yaml
    Y podemos ver los objetos creados con el siguiente comando:
    -> kubectl get all
    Y si necesitamos controlar en tiempo real los pods, y queremos hacer una prueba como borrar un pod haver si se vuelve a contruir de la misma forma ejecutaremos los siguientes comandos:
    -> watch kubectl get po
    -> kubectl delete pod mongod-0

• Realizar  una  operación  en  una  de  las  instancias  a  nivel  de  configuración  y verificar que el cambio se ha aplicado al resto de instancias 
    - Para esta práctica lo que haremos será entrar en el pod que hace de master, crear una base de datos, una colección llamada restaurantes e insertaremos algún dato para ver si persiste en el tiempo.
    Primeramente entramos en el pod "master" o "primary" con el siguiente comando:
    -> kubectl exec -it mongod-2 bash
    Una vez dentro nos metemos en la consola de mongo con el siguiente comando:
    -> mongosh
    Y una vez en la consola de mongo ejecutaremos los siguientes comandos para realizar la inserción de datos:
    -> use newDatabaseKubernetesMongo;
    -> db.createCollection('restaurantes');
    -> db.restaurantes.insertOne({'name': 'McDonalds'});
    Por último ejecutamos un comando para ver si se ha insertado correctamente y que nos devuelva el dato insertado previamente:
    -> db.restaurantes.find({});

    - En este punto pasamos a borrar el pod que hace de "master" y si todo va bien veremos como uno de los pods "slaves" pasa a ser "master" y persiste la información porque la ha replicado:
    -> kubectl delete pod mongod-2
    Entramos en otro pod y vemos si persiste la información:
    -> kubectl exec -it mongod-1 bash
    -> db.restaurantes.find({});
    Esto nos devuelve el resultado y confirma que todo funciona correctamente.
    
    - En el pdf de la carpeta "answer_exercise_2/" se encuentran las capturas de todo el proceso.


• Diferencias  que  existiría  si  el  montaje  se  hubiera  realizado  con  el  objeto  de ReplicaSet 

    - Para empezar habriamos perdido el almacenamiento estable y persistente en el tiempo, ya que el ReplicaSet solo tiene como mision conseguir tener el servicio levantado el máximo tiempo posible y si se cae que sea con la menor brevedad posible. Como no tiene asignado un volumen de persistencia específico al que replicar, podríamos perder la persistencia y seguridad de los datos de nuestra base de datos. 
    Lo segundo es que perderíamos los idetifícadores únicos de nuestros pods, por lo que no podríamos organizar un sistema de "master" y "slaves" por que no podríamos predecir el nombre de los pods. Esto provoca que si cae el "master", el sistema no sabría que pod que actuaba como "slave" podría pasar a master.
    El tercer punto, es que el despliegue no sería ordenado ni controlado, por lo que el sistema solo se centraría en levantar el pod base una vez uno caiga con un nombre aleatorio y sin la información persistida.
    Por lo tanto, la principal diferencia entre los dos para saber cual elegir es: si necesitas desplegar una aplicación con un controlador que proporcione réplicas controladas con estado eligirás el StatefulSet. En cambio, si las replicas que se pretenden controlar son sin estado y el despliegue no necesita de un orden ni identificadores únicos, eligiremos el ReplicaSet con un nivel más alto de controlador como sería el objeto de tipo Deployment.

- En el archivo .pdf de la carpeta "answer_exercise_2/" se encuentra el resto de capturas de pantalla con lo que se ha llevado a cabo.