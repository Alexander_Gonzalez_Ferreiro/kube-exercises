1. [Ingress Controller / Secrets] Crea los siguientes objetos de forma declarativa con las siguientes especificaciones: 
 
    • Imagen: nginx 
    • Version: 1.19.4 
    • 3 replicas 
    • Label: app: nginx-server 
    • Exponer el puerto 80 de los pods 
    • Limits: 
    CPU: 20 milicores   
    Memoria: 128Mi 
    
    • Requests: 
    CPU: 20 milicores 
    Memoria: 128Mi

- En la carpeta "answer_exercise_1/" se encuentra el archivo answer_exercise_1.yaml y el documento .pdf con todas las capturas del proceso.

Primeramente empezaremos por crear el yaml que contendrá nuestro ingress, nuestro service y el objeto ReplicaSet para la realización del ejercicio.

a. A  continuación,  tras  haber  expuesto  el  servicio  en  el  puerto  80,  se  deberá acceder a la página principal de Nginx a través de la siguiente URL:

- Para hacer esto primeramente habilitaremos un plugin para poder utilizar el ingress, ejecutamos el siguiente comando:

  -> minikube addons enable ingress

  Una vez habilitado esto, ejecutamos lo siguiente para crear los objetos:
  -> kubectl apply -f answer_exercise_1.yaml

  Ejecutamos -> minikube ip , para saber la ip a la que conectarnos y sobreescribimos el archivo hosts de windows con la ip de minikube apuntado a "alexander.student.lasalle.com".

  Esto me resulto imposible de realizar ya que no se consiguió conectar con la ip de minikube ni reinstalando minikube ni cambiando el vm-driver del cluster de minikube 
  ni reiniciando los servicios. Por lo tanto se procedio a ejecutar lo siguiente:
  -> minikube tunnel
  Que era lo que recomendaba minikube al ejecutar el "enable ingress".
  Y se volvió a sobrescribir el archivo hosts.

  Por último, se validó si funcionaba buscando en firefox: alexander.student.lasalle.com.

  Y se acabó de validar con el siguiente comando:

  -> curl -k http://alexander.student.lasalle.com
  Que devolvió el index.html de nginx.

b. Una vez realizadas las pruebas con el protocolo HTTP, se pide acceder al 
servicio mediante la utilización del protocolo HTTPS, para ello: 
 
• Crear un certificado mediante la herramienta OpenSSL u otra similar 

- Para crear el certificado se uso el WSL (Windows Subsystems for Linux) con OpenSSL para crear el certificado, con el siguiente comando:
  -> openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout keyFile
  -out certFile -subj “/CN=alexander.student.lasalle.com/O=alexander.student.lasalle.com"
  Los recursos resultantes se pueden encontrar en la carpeta "answer_exercise_1/" con el nombre de certFile.crt para el certificado y keyFile.key para la clave.


• Crear un secret que contenga el certificado 

- El certificado lo creamos con el siguiente comando:
  -> kubectl create secret tls alexander.student.lasalle.com --key keyFile --cert certFile
  Y lo añadiremos a nuestro ingress agregando lo siguiente a nuestro "answer_exercise_1.yaml":

  spec:
    tls:
      - hosts:
        - alexander.student.lasalle.com
          secretName: alexander.student.lasalle.com


  Por último verificaremos en el navegador entrando a https://alexander.student.lasalle.com y aceptando que es un certificado autofirmado. 
  Y analizaremos el certificado para ver que tiene el siguiente nombre: alexander.student.lasalle.com

  Y ya como validación final ejecutaremos:
  -> curl --cacert alexander.student.lasalle.com https://alexander.student.lasalle.com
  Y nos devolverá el código de index.html de nginx.

- En el archivo .pdf de la carpeta "answer_exercise_1/" se encuentra el resto de capturas de pantalla con lo que se ha llevado a cabo.