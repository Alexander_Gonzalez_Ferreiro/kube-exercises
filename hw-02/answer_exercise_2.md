2. [2  ptos]  Crear  un  objeto  de  tipo  replicaSet  a  partir  del  objeto  anterior  con  las siguientes especificaciones:
    
  - En la carpeta "answer_exercise_2/" se encuentra el archivo answer_exercise_2.yaml y el documento
  .pdf con todas las capturas del proceso.

  • Debe tener 3 replicas 
    Para especificar esto en el archivo answer_exercise_2.yaml le especificamos lo siguiente:
    -> replicas: 3
    Teniendo en cuenta que es un ReplicaSet generará 3 pods.

  • ¿Cúal sería el comando que utilizarías para escalar el número de replicas a 10? 
    Utilizariamos el siguiente comando:
    -> kubectl scale --replicas=10 rs nginx
    
  • Si necesito tener una replica en cada uno de los nodos de Kubernetes, ¿qué objeto se adaptaría mejor? (No es necesario adjuntar el objeto)

    En este punto estamos hablando de la diferéncia entre lo que sería el ReplicaSet y el DaemonSet.
        
    La diferencia es que el ReplicaSet se asegurará de levantar el número de pods definido en nuestro archivo de configuración (.yaml) y de que 
    siempre esten corriendo en nuestro cluster. En este caso los pods se alojarán o levantaran en los nodos segun sus recursos disponibles. Si 
    uno de estos nodos cae o se elimina, caeran todos los pods de ese nodo y el ReplicaSet volverá a levantar las replicas correspondientes.

    En el caso del DaemonSet, se asegurára de levantar los pods especificados en el archivo de configuración (.yaml), pero levantará un pod por 
    nodo. Es decir, el número total de pods será igual al número de nodos "workers" en el cluster. Si añadimos un nuevo nodo, el DaemonSet se 
    encargará de levantar un pod en ese nuevo nodo. Pero si, se elimina o cae uno de los nodos el DaemonSet no levantará más pods para compensar.

    En conclusión, si necesitas tener un pod por nodo lo mejor es usar el DaemonSet ya que el se asegurará de que esto se cumpla, pero hay que 
    tener en cuenta que si un nodo cae no te replicará el pod perdido en otro nodo, solo estará atento a la creación de nuevos nodos para añadirles
    el pod configurado.

    - En el archivo .pdf de la carpeta "answer_exercise_2/" se encuentra el resto de capturas de 
    pantalla con lo que se ha llevado a cabo.