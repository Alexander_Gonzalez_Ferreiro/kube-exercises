5. [2 ptos] Diseña una estrategia de despliegue que se base en ”Blue Green”. Podéis utilizar la imagen del ejercicio 1.
    a. Existe una aplicación que está desplegada en el clúster (en el ejemplo, 1.0v).
    b. Antes de ofrecer el servicio a los usuarios, la compañía necesita realizar una serie de validaciones con la 
    versión 2.0. Los usuarios siguen accediendo a la versión 1.0.
    c. Una vez que el equipo ha validado la aplicación, se realiza un switch del tráfico a la versión 2.0 sin impacto 
    para los usuarios.

    - En la carpeta "answer_exercise_5/" se encuentra los archivos blue-v1.yaml, green-v2.yaml 
    y patch-file-service.yaml y el documento .pdf con todas las capturas del proceso.

    Para la realización del ejercicio primeramente hemos elaborado tres yaml. El primero blue-v1.yaml que crea un servicio
    y un objeto deployment con nuestra versión 1 de la aplicación, el segundo green-v2.yaml que especifica la versión 2 de
    nuestra aplicación y el archivo patch-file-service.yaml que servirá para hacer el cambio de una versión a otra. En las 
    dos versiones de nuestra aplicación se ha integrado las pruebas de vida (livenessProbe) y la de estar listo (readinessProbe).

    El objetivo es conseguir redirigir todas las peticiones de nuestra aplicación de una versión a otra con zero downtime
    en producción. Sin que en ningún momento nuestra aplicación no conteste.

    a) A partir de aqui, primero levantamos el servicio y el deployment con el siguiente comando:
    -> kubectl apply -f blue-v1.yaml
    Ejecutamos el siguiente comando para asegurarnos de que ha levantado la versión uno y ver las ips de los pods levantados:
    -> kubectl descrive svc nginx-service
    -> kubectl get pods -o wide
    Lo siguiente es ejecutar un comando para ver el estado de los pods en tiempo real:
    -> watch kubectl get po
    Como hemos establecido que nuestro servicio es de tipo NodePort creamos un enlace de puertos entre el nodo de minikube y 
    nuestro localhost para poder ver que se sigue pudiendo visualizar el index.html de nginx:
    -> minikube service nginx-service --namespace=kube-exercises

    b) En este punto llegamos al momento de los testers, por lo tanto levantamos la versión 2 de nuestra aplicación pero sin 
    modificar el tráfico:
    -> kubectl apply -f green-v2.yaml
    De momento todo sigue igual. Corren los 6 pods en paralelo, y estan todos levantados.
    Entonces, con el siguiente comando utilizaremos el archivo patch-file-service.yaml para parchear nuestro servicio y redirigir
    el tráfico de nuestra aplicación a la versión 2:
    -> kubectl patch service nginx-service --patch $(Get-Content patch-file-service.yaml -Raw)
    Y si ejecutamos el siguiente comando:
    -> kubectl descrive svc nginx-service
    Veremos como todas las ips de nuestro servicio han cambiado a las de los pods con la nueva version. De hecho podemos eliminar las
    antiguas:
    -> kubectl delete deploy nginx-deployment-v1
    Y si vamos al localhost con el puerto que hemos abierto antes para enlazar con minikube veremos que todo sigue funcionando.

    - En el archivo .pdf de la carpeta "answer_exercise_5/" se encuentra el resto de capturas de 
    pantalla con lo que se ha llevado a cabo.