3. [2  ptos] Crea  un  objeto  de  tipo  service  para  exponer  la  aplicación  del  ejercicio anterior de las siguientes formas: 
  
  - En la carpeta "answer_exercise_3/" se encuentra los archivos service1, service2 y service3.yaml y el documento
  .pdf con todas las capturas del proceso.
  
  • Exponiendo el servicio hacia el exterior (crea service1.yaml) 
    En este punto estamos hablando de un servicio de tipo LoadBalancer que lo que hace es exponer una ip privada del 
    cluster hacia el exterior a través de una ip pública, que se suele resolver con una dns. Es decir, este tipo de servicio 
    se suele usar en servidores como Digital Ocean que redirige al servicio creado en el cluster de kubernetes.

    En mi caso he creado el service1.yaml con la especificación de:
    -> type: LoadBalancer
    Ejecutamos la siguiente instrucción:
    -> kubectl create -f service1.yaml
    Y si ejecutamos un get para ver la congiguración del servicio:
    -> kubectl get svc
    Nos encontraremos que la columna de "EXTERNAL-IP" esta en <pending>, ya que nos tiene una ip pública a la que apunte.
    Por lo tanto con la finalidad del ejercicio, ejecute la siguiente instrucción para que desde el localhost del navegador
    me resuelva la ip del servicio creado:
    -> minikube tunnel
    Esto resolvera la ip de localhost contra la del cluster ip de minikube, pudiendo visualizar el contenido del index.html
    del nginx en el puerto correspondiente.
    En mi caso además, modifique el archivo hosts de windows para resolver localhost como nginx-service para poder buscar
    nginx-service en el navegador.

  • De forma interna, sin acceso desde el exterior (crea service2.yaml) 
    En este caso el tipo de servicio que estamos buscando es un service de tipo ClusterIP, para que solo se pueda acceder a la
    red del servicio desde dentro y que nadie más que no pertenezca a esta red pueda acceder.

    En mi caso he creado el service2.yaml con la especificación de:
    type: ClusterIP
    Ejecutamos la siguiente instrucción:
    -> kubectl create -f service2.yaml
    Y con la finalidad del ejercicio, podemos entrar dentro de un pod y ver si resuelve las ips internamente. Para ello ejecutamos
    lo siguiente:
    -> kubectl exec -it <nombre-pod> sh
    Y realizamos un ping a nginx2-service, que es nuestro servicio, y debería resolver la ip del servicio:
    -> ping nginx2-service
    Por último, podemos ejecutar un curl desde el pod y veremos que el servicio nos resuelve la dirección de uno de los pods y 
    nos devuelve el index.html del nginx de uno de estos:
    -> curl http://nginx2-service:80
  
  • Abriendo un puerto especifico de la VM (crea service3.yaml) 
    En este caso el tipo de servicio que estamos buscando es un service de tipo NodePort, para que exponga un puerto específico de 
    cada nodo hacia el exterior o en nuestro caso hacia nuestra máquina amfitrión.

    En mi caso he creado el service3.yaml con la especificación de:
    type: NodePort
    Ejecutamos la siguiente instrucción:
    -> kubectl create -f service3.yaml
    Y con la finalidad del ejercicio ejecutamos la siguiente instrucción para crear una vpn o tunnel, en realidad para especificar
    que nuestro localhost con un puerto específico apunta al nodo de minikube con el puerto que especificamos en el NodePort:
    -> minikube service nginx3-service --namespace=kube-exercises
    En este punto podremos visualizar el index.html del nginx de uno de nuestros pods desde el localhost accediendo al puerto del 
    enlace entre nuestra máquina y el nodo de minikube.

  - En el archivo .pdf de la carpeta "answer_exercise_3/" se encuentra el resto de capturas de 
    pantalla con lo que se ha llevado a cabo.