4. [2 ptos] Crear un objeto de tipo deployment con las especificaciones del ejercicio 1. 

  - En la carpeta "answer_exercise_4/" se encuentra los archivos answer_exercise_4_A_v1.yaml,  
  answer_exercise_4_A_v2.yaml, answer_exercise_4_B.yaml y aux-service.yaml y el documento
  .pdf con todas las capturas del proceso.

  • Despliega  una  nueva  versión  de  tu  nuevo  servicio  mediante  la  técnica “recreate” 
    Al objeto de tipo deployment le hemos especificado lo siguiente para que use la técnica
    recreate:
    -> strategy: type: Recreate
    Primeramente levantamos el objeto junto con un servicio auxiliar:
    -> kubectl apply -f answer_exercise_4_A_v1.yaml
    -> kubectl apply -f aux-service.yaml
    Levantamos una pasarela con el nodo de minikube para acceder al servicio auxiliar de tipo
    NodePort:
    -> minikube service nginx-service --namespace=kube-exercises
    Y esto ya nos permite visualizar el index.html del nginx de uno de nuestros pods. El siguiente
    paso era levantar la misma aplicación otra vez, en nuestro caso hemos creado un archivo yaml que 
    sobreescriba el objeto con un label de version 2. El archivo es "answer_exercise_4_A_v2.yaml".
    Por lo tanto ejecutamos el siguiente comando:
    -> kubectl apply -f answer_exercise_4_A_v2.yaml
    Y si ejecutamos el siguiente comando antes de levantar la versión 2:
    -> watch kubectl get po
    Con este comando podremos observar como el deployment empieza a eliminar los pods de la versión 
    anterior y empieza a levantar los de la nueva versión. Esto implica un tiempo en que la aplicación no
    está activa para los usuarios.

  • Despliega una nueva versión haciendo “rollout deployment” 
    Al objeto de tipo deployment le hemos especificado lo siguiente para que use la técnica
    RollingUpdate con los siguientes parametros:

    strategy:
      type: RollingUpdate
      rollingUpdate:
        maxSurge: 25%
        maxUnavailable: 25% 

     Primeramente levantamos el objeto junto con un servicio auxiliar:
    -> kubectl apply -f answer_exercise_4_B.yaml
    -> kubectl apply -f aux-service.yaml
    Hacemos una modificación en la imagen del contenedor por ejemplo, con el siguiente comando:
    -> kubectl set image deploy nginx-deployment nginx-app=nginx:1.19.5
    Y ejecutamos el siguiente comando podremos ver como va levantando nuevas replicas y va eliminado las antiguas
    sin eliminar todas las antiguas de golpe y permitiendo que la aplicación permanezca siempre levantada:
    -> kubectl rollout status deploy nginx-deployment

  • Realiza un rollback a la versión generada previamente
    Partiendo del punto anterior si ejecutamos la siguiente instrucción se volverra a la version anterior haciendo
    un rollback:
    -> kubectl rollout undo deployment nginx-deployment
    Y ejecutamos el siguiente comando podremos ver como va levantando las replicas antiguas y va eliminado las nuevas
    sin eliminar todas las nuevas de golpe y permitiendo que la aplicación permanezca siempre levantada:
    -> kubectl rollout status deploy nginx-deployment

  - En el archivo .pdf de la carpeta "answer_exercise_4/" se encuentra el resto de capturas de 
  pantalla con lo que se ha llevado a cabo.