1. [2 ptos] Crea un pod de forma declarativa con las siguientes especificaciones: 
Realiza un despliegue en Kubernetes, y responde las siguientes preguntas:

    - En la carpeta "answer_exercise_1/" se encuentra el archivo answer_exercise_1.yaml y el documento
    .pdf con todas las capturas del proceso.

    - Primeramente crearemos el archivo .yaml con las especificaciones que se demandaban y ejecutaremos
    la siguiente instrucción:
    -> kubectl apply -f answer_exercise_1.yaml.

    • ¿Cómo puedo obtener las últimas 10 líneas de la salida estándar (logs generados por la aplicación)? 
        - El comando para realizar esta acción sería el siguiente: 
        -> kubectl logs -f --tail 10 nginx

    • ¿Cómo podría obtener la IP interna del pod? Aporta capturas para indicar el proceso que seguirías. 
        - Podemos ejecutar uno de los dos siguientes comandos:
        -> kubectl get pod nginx -o wide
        -> kubectl describe pod nginx
        Cualquiera de estos dos comandos nos sacará por pantalla la IP que esta utilizando el Pod.

    • ¿Qué comando utilizarías para entrar dentro del pod? 
        - Utilizaría el siguiente comando:
        -> kubectl exec -it nginx sh

    • Necesitas  visualizar  el  contenido  que  expone  NGINX,  ¿qué  acciones debes llevar a cabo? 
        - Una de las soluciones sería exponer el puerto del pod contra uno de los puertos de la máquina
        anfitrión, por lo cual con el siguiente comando:
        -> kubectl port-forward nginx 8080:80
        Con este comando exponemos el puerto 80 del pod contra el puerto 8080 de la maquina anfitrión
        y podemos visualizar el contenido que expone NGINX.

    • Indica la calidad de servicio (QoS) establecida en el pod que acabas de crear. ¿Qué lo has mirado?
        - Para este caso ejecutaremos el siguiente comando que nos devolvera la información detallada
        del pod en formato yaml:
        -> kubectl get pod nginx --namespace=kube-exercises --output=yaml
        Esto nos devolvera toda la información y si nos fijamos en una en concreta pondrá:
        -> qosClass: Guaranteed
        Esto significa que las peticiones y los limites son iguales, estan balanceados en cuanto a gasto del equipo.
        También, si hubieramos creado otro pod con una configuración distinta nos podría haber dicho que el "qosClass"
        es igual a "Burstable" (peticiones y límites no son iguales) o "Best Effort" (peticiones y límites no han sido
        definidos). Al final esto viene definido, en parte, por la configuración que le definamos al pod en el gasto
        de recursos.

    - En el archivo .pdf de la carpeta "answer_exercise_1/" se encuentra el resto de capturas de 
    pantalla con lo que se ha llevado a cabo.